// pages/order/order.js
const {orderStatusMap} = require('../../utils/public.js');
var navIdOut = "";
var pageNum = 0;
var navIdx = 0;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // form: {
    //   orderId: '',
    //   sellmoney: 0,
    //   tcmony:0,
    //   cjdate: ''
    // }
    orderList: [],
    navIdx: 0,
    scrLeft: 0,
    minHit: 0,
    wkLists: [],
    onLoaded: false,
    

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },



  // 获取活动距离
  getScrLeft(navIdx) {
    var itemWth = wx.getSystemInfoSync().windowWidth / 5
    var scrLeft = (navIdx - 2) * itemWth
    if (scrLeft < 0) {
      scrLeft = 0
    }
    this.setData({
      scrLeft
    })
  },
  //新增收货地址
  onAddAddr: function (e) {
    wx.navigateTo({
      url: '/pages/order/order?status=0',
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let userInfo = wx.getStorageSync('userInfo')
    this.setData({
      nickname: userInfo.nickName, // 微信昵称
      // 微信头像
    })
    this.clickNav({
      navIdx,
      navIdOut
    });

    this.getNavData()
    //获取最新宽度
    this.getWinHit()
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      wkLists:[]
    })
    // console.log("执行=================================");
    this.getOrderList(navIdOut)


  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    pageNum = this.data.orderList.length
    this.getOrderList(navIdOut,pageNum,7)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  // 修改订单
  toEditOrder: function (e) {
    let id = e.currentTarget.dataset.orderid;
    wx.navigateTo({
      url: '/pages/order/order?status=1&orderId=' + id,
    })
  },


  formSubmit(e) {
    console.log('form发生了submit事件，携带数据为：', e.detail.value)
    this.setData({
      allValue: e.detail.value
    })
  },

  formReset(e) {
    console.log('form发生了reset事件，携带数据为：', e)
    this.setData({
      allValue: ''
    })
  },
  bindDateChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      createDate: e.detail.value
    })
  },


  //点击导航特效
  clickNav(res) {
    // wx.showLoading({
    //   title: '数据加载中...',
    //   mask: true
    // })

    if (res.currentTarget) {
      var navIdx = res.currentTarget.dataset.idx
      var navId = res.currentTarget.dataset.id
    } else {
      var navIdx = res.navIdx
      var navId = res.navId
    }

    navIdOut = navId;

    this.setData({
      navIdx,
      navId,
      orderList: []
    })
    // this.setData({
    //   onLoaded: false
    // })
    this.getOrderList(navId)



  },
  //获取最小高度
  getWinHit() {
    var winHit = wx.getSystemInfoSync().windowHeight - 160
    this.setData({
      minHit: winHit
    })
  },

  //获取真实的订单列表
  getOrderList(classid = 17, page = 0, num = 7) {
    pageNum = page;
    this.setData({
      onLoaded: true
    })
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })

    wx.cloud.callFunction({
      name: 'getOrderList',
      data: {
        orderStatus: classid,
        pageNum: num,
        pageSize: page,
        nickname: this.data.nickname
      }
    })
    .then(res=> {
        // console.log(res);
        const result = res.result
        const data = result.data
        if (data.length == 0 || data.length < 6) {
          this.setData({
            onLoaded: false
          })
        }
        data.forEach(e => {
          e.orderStatusName = orderStatusMap.get(e.orderStatus)
        })
        let oldList = this.data.orderList;
        let newList = oldList.concat(data)
        this.setData({
          orderList: newList
        })
        wx.hideLoading()
        wx.stopPullDownRefresh()

    })

 /**

    const param = {
      data: {
        orderStatus: classid,
        pageNum: page,
        pageSize: num,
        nickname: this.data.nickname
      },
      url: '/app/order/page',
      method: 'GET',
      callBack: (res) => {
        // console.log(res);
        if (res.orders.length == 0 || res.orders.length < 6) {
          this.setData({
            onLoaded: false
          })
        }
        // res.data.forEach(item => {
        //   item.title = common.getStrLen(item.title, 25)
        // })
       
        let oldList = this.data.orderList;
        let newList = oldList.concat(res.orders)
        this.setData({
          orderList: newList
        })
        wx.hideLoading()
        wx.stopPullDownRefresh()

      }

    }
    http.request(param)
**/
  },

  //获取真实的导航列表内容
  getNavData() {
    // wx.request({
    //   url: 'https://ku.qingnian8.com/school/infoclass.php',
    //   success:res=>{       
    this.setData({
      navLists: [{
        id: 17,
        classname: '全部'
      }, {
        id: 0,
        classname: '已付款'
      }, {
        id: 1,
        classname: '已完成'
      }, {
        classname: '已结算',
        id: 2
      }]
    })
    this.getScrLeft(navIdx)
    // }
    // })

  },

})