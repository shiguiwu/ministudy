// pages/order/order.js

const util = require('../../utils/util.js');
// const http = require('../../utils/http.js');
// const db = wx.cloud.database()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // form: {
    //   orderId: '',
    //   sellmoney: 0,
    //   tcmony:0,
    //   cjdate: ''
    // }
    allValue: '',
    createDate: util.formatDate(new Date()),
    mobile: '18676894410',
    // nickname: '',
    // avatarUrl: '',
    // title: '',
    orderId: '-1',
    // amonut: 1000,
    // username: '',
    // price: '',
    // studentCommission: '',
    priceCommission: '80',
    status: 0,
    mjtc: '0.00￥',
    projc: '0.00￥'



  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 查看是否授权
    let id = options.orderId;
    this.setData({
      status: options.status,
    })
    if (!id) {
      return;
    }
    this.getDetail(id)

  },

  getDetail(id) {
    wx.showLoading();
    wx.cloud.callFunction({
        name: 'detail',
        data: {
          id: id
        }
      })
      .then(res => {
        // console.log(res);
        const {
          result
        } = res
        const {
          data
        } = result
        this.setData({
          amount: data.amount,
          orderSn: data.orderSn,
          price: data.price,
          username: data.username,
          studentCommission: data.studentCommission,
          priceCommission: data.priceCommission,
          mobile: data.mobile,
          // nickname,
          orderId: data._id,
          title: data.title,
          createDate: data.createDate

        })
        this.computerTc(data.priceCommission, data.price, 'mjtc')
        this.computerTc(data.studentCommission, data.price, 'projc')
        wx.hideLoading()
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // wx.showLoading();
    // console.log("------------------------onShow");
  },
  //保存订单
  onSaveOrUpdateOrder(e) {
    let amount = this.data.amount;
    let orderSn = this.data.orderSn;
    let price = this.data.price;
    let mobile = this.data.mobile;
    let orderStatus = e.currentTarget.dataset.status;
    //^1[34578]\d{9}$/.test(phone)
    // let userInfo = getApp().globalData.userInfo;
    let userInfo = wx.getStorageSync('userInfo');
    // console.log(nickname)
    // console.log(userInfo)
    if (!amount) {
      wx.showToast({
        title: '请输入订单价格',
        icon: "none"
      })
      return;
    }

    if (!orderSn) {
      wx.showToast({
        title: '请输入订单号',
        icon: "none"
      })
      return;
    }

    if (!price) {
      wx.showToast({
        title: '请输入技术价格',
        icon: "none"
      })
      return;
    }
    if (mobile && !/^1[34578]\d{9}$/.test(mobile)) {
      wx.showToast({
        title: '请输入正确的支付宝账号',
        icon: "none"
      })
      return;
    }
    wx.showLoading();

    let id = this.data.orderId
    const param = {
      amount,
      orderSn,
      price,
      username: this.data.username,
      studentCommission: this.data.studentCommission,
      priceCommission: this.data.priceCommission,
      mobile: this.data.mobile,
      nickname: userInfo.nickName,
      title: this.data.title,
      createDate: this.data.createDate,
      orderStatus: orderStatus
    }

    //发送请求
    // http.request(param)
    wx.cloud.callFunction({
      name: 'saveOrupdate',
      data: {
        obj: param,
        id: id
      }
    }).then(res => {
      wx.hideLoading()
      wx.navigateBack({
        delta: 1
      })

    })
    // db.collection('order').add({data:param}).then(res => console.log(res));
  },

  onDeleteOrder() {
    wx.showLoading();
    wx.cloud.callFunction({
      name:'deleteOrder',
      data: {
        _id: this.data.orderId
      }
    })
    .then(res => {
      wx.hideLoading();
      wx.navigateBack({
        delta: 1
      })
    })
   
    //发送请求
   
  },

  showCollectionCount: function () {
    var ths = this;
    wx.showLoading();
    var params = {
      url: '/p/user/collection/count?nickname=' + this.data.nickname,
      method: "GET",
      data: {},
      callBack: function (res) {
        wx.hideLoading();
        ths.setData({
          collectionCount: res
        });
      }
    };
    http.request(params);
  },
  //处理填写订单
  onOrderSnInput(e) {
    this.setData({
      orderSn: e.detail.value
    });
  },

  //处理填写需求
  onTitleInput(e) {
    this.setData({
      title: e.detail.value
    });
  },

  //处理金额
  onAmonutInput(e) {
    let amount = e.detail.value
    this.computerTc(this.data.priceCommission, amount, 'mjtc')
    this.computerTc(this.data.studentCommission, amount, 'projc')
    this.setData({
      amount,
      price: amount
    });
  },

  //处理日期
  bindDateChange(e) {
    this.setData({
      createDate: e.detail.value
    });
  },
  computerTc(rito, money, param) {
    if (money && rito) {
      let c = Number.parseFloat(rito) / 100
      // c = Number.toFixed(2)
      c = (money * c).toFixed(2) + "￥"
      this.setData({
        [param]: c
      });
    }
  },
  //处理学生提成
  onStudentCommission(e) {
    let studentCommission = e.detail.value
    this.computerTc(studentCommission, this.data.price, 'projc')
    this.setData({
      studentCommission
    });
  },

  //处理填写技术员名称
  onUsernameInput(e) {
    this.setData({
      username: e.detail.value
    });
  },

  //处理价格
  onPriceInput(e) {
    let price = e.detail.value;
    this.computerTc(this.data.priceCommission, price, 'mjtc')
    this.computerTc(this.data.studentCommission, price, 'projc')
    this.setData({
      price
    });
  },

  //处理技术提成
  onPriceCommission(e) {
    let priceCommission = e.detail.value
    this.computerTc(priceCommission, this.data.price, 'mjtc')
    this.setData({
      priceCommission
    });
  },

  //处理价格
  onMobileInput(e) {
    this.setData({
      mobile: e.detail.value
    });
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },


  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },


  formSubmit(e) {
    console.log('form发生了submit事件，携带数据为：', e.detail.value)
    this.setData({
      allValue: e.detail.value
    })
  },

  formReset(e) {
    console.log('form发生了reset事件，携带数据为：', e)
    this.setData({
      allValue: ''
    })
  },

})