// pages/my/my.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    menuitems: [
      { text: '操作指引', url: '../guide/guide', icon: '/images/icon/toDelivery.png', arrows: '/images/arrows.png' },
      // { text: '清空记录', url: '#', icon: '/images/user3.png', arrows: '/images/arrows.png' },
      { text: '关于作者', url: '../about/about', icon: '/images/tabbar/user-sel.png', arrows: '/images/arrows.png' }
    ]

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  toDistCenter() {
    wx.navigateTo({
      url: '/pages/guide/guide',
    })
  },
  toCouponCenter() {
    wx.navigateTo({
      url: '/pages/about/about',
    })
  },
  toOrderListPage() {
    wx.redirectTo({
      url: '/pages/orderList/orderList',
    })
  }
})