// const http = require('../../utils/http.js');
const {formatMonth,formatDate} = require('../../utils/util.js');

Page({
  data: {
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    nickname: '',
    countOrder: {},
    countMonth: {},
    logs: [],
    dayList: [],
    list: [],
    activeIndex: 0,
    index: 0,
    sum: [{
        title: '今日出单量',
        val: '0'
      },
      {
        title: '本月累积出单量',
        val: '0'
      },
      {
        title: '近三个月出单量',
        val: '0分钟'
      },
      {
        title: '近六个月出单量',
        val: '0分钟'
      }
    ],
    cateArr: [{
        icon: 'work',
        text: '工作'
      },
      {
        icon: 'study',
        text: '学习'
      },
      {
        icon: 'think',
        text: '思考'
      },
      {
        icon: 'write',
        text: '写作'
      },
      {
        icon: 'sport',
        text: '运动'
      },
      {
        icon: 'read',
        text: '阅读'
      }
    ]
  },
  onLoad(options) {
    let userInfo = wx.getStorageSync('userInfo')
    this.setData({
      nickname: userInfo.nickName, // 微信昵称
      // 微信头像
    })
  },
  getCountOrder: function () {
    wx.showLoading();
    wx.cloud.callFunction({
      name: 'countMonth',
      data: {
        currentMonth: formatMonth(new Date()),
        nickname: this.data.nickname
      }
    })
    .then(res => {
      const result = res.result
      // console.log(res);
      this.setData({
        countMonth: result
      })
      wx.hideLoading();
    })

   
  },

  getCountMonth: function () {
    wx.showLoading();
    wx.cloud.callFunction({
      name: 'currentMonth',
      data: {
        last: formatMonth(new Date(),0,-1),
        nickname: this.data.nickname
      }
    })
    .then(res => {
      const result = res.result
      // console.log(res);
      const list = result.list
      const currentDateStr = formatDate(new Date())
      //当日出单量
      const current = list.filter(e => e.formatDate === currentDateStr).length

      const sevenDateStr = formatDate(new Date(),0,-7)
      const sevenDay =  list.filter(e => new Date(e.formatDate) >= new Date(sevenDateStr)).length

      const monthCount =  list.filter(e => e.formatDate.slice(0,7) === currentDateStr.slice(0,7)).length

     const lastMonthStr =  formatMonth(new Date(),0,-1)
     const lastMonthCount =  list.filter(e => e.formatDate.slice(0,7) ===lastMonthStr).length
      this.setData({
        countOrder: {
          current,
          sevenDay,
          monthCount,
          lastMonthCount
        }
      })
      wx.hideLoading();
    })




  },
  onShow: function () {
    this.getCountOrder()
    this.getCountMonth()

  },
  changeType: function (e) {
    var index = e.currentTarget.dataset.index;
    if (index == 0) {
      this.setData({
        list: this.data.dayList
      })
    } else if (index == 1) {
      this.setData({
        list: this.data.logs
      })
    }
    this.setData({
      activeIndex: index
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    wx.showShareMenu({
      withShareTicket: true
    })
  }
})