const formatDate = (date,m=0,d=0) => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1 + m
  const day = date.getDate() + d

  return `${[year, month, day].map(formatNumber).join('-')}`
}
const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  let dateStr =  `${[year, month, day].map(formatNumber).join('-')}`
  let timeStr = `${[hour, minute, second].map(formatNumber).join(':')}`
  return `${dateStr} ${timeStr}`
}

const formatMonth = (date,y=0,m=0) => {
  const year = date.getFullYear() +y
  const month = date.getMonth() + 1 +m
  return `${[year, month].map(formatNumber).join('-')}`
}
const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : `0${n}`
}

module.exports = {
  formatTime,formatDate,formatMonth
}
