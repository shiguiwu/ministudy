// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()
const $ = db.command.aggregate

const _ = db.command

// 云函数入口函数
exports.main = async (event, context) => {
  const {
    currentMonth: curr,
    nickname
  } = event
  // console.log("add----------");
  const {
    list
  } = await db.collection('order-list')
    .aggregate()
    .addFields({
      formatDate: $.dateToString({
        date: '$createTime',
        format: '%Y-%m',

      })
    })
    .match({
      formatDate: _.eq(curr),
      nickname: _.eq(nickname)
    })
    .project({
      _id: 0,
      orderStatus:1,
      formatDate: $.dateToString({
        date: '$createTime',
        format: '%Y-%m',

      })

    })
    .end()
    const pay = list.filter(e => e.orderStatus ===0).length
    const finish = list.filter(e => e.orderStatus ===1).length
    const settle = list.filter(e => e.orderStatus ===2).length
  return {
    pay,
    finish,
    settle,
    total: list.length
  }
}