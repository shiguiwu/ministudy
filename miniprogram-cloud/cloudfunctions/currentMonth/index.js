// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()
const $ = db.command.aggregate

const _ = db.command

// 云函数入口函数
exports.main = async (event, context) => {
  const {
    last,
    nickname
  } = event
  // console.log("add----------");
  return await db.collection('order-list')
    .aggregate()
    .addFields({
      formatDate: $.dateToString({
        date: '$createTime',
        format: '%Y-%m',

      })
    })
    .match({
      formatDate: _.gte(last),
      nickname: _.eq(nickname)
    })
    .project({
      _id: 0,
      formatDate: $.dateToString({
        date: '$createTime',
        format: '%Y-%m-%d',

      })

    })
    .end()
   
}