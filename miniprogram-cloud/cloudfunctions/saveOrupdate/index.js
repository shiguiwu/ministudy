// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const {obj,id} = event
  if (id === '-1') {
    // console.log("add----------");
    return await db.collection('order-list').add({
      data: {
        ...obj,
        updateTime: new Date(),
        createTime: new Date(),
      }
    })
  } else {
    //   console.log("update----------");
    return await db.collection('order-list').doc(id).update({
      data: {...obj, updateTime: new Date()}
    })

  }
}