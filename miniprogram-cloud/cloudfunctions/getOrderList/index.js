// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {

  const {
    pageNum,
    pageSize,
    orderStatus,
    nickname
  } = event

  const param = {}
  if (orderStatus !== 17) {
    param.orderStatus = orderStatus
  }
  param.nickname = nickname
  // console.log("add----------");
  return await db.collection('order-list').where({
    ...param
  }).orderBy('updateTime', 'desc').skip(pageSize).limit(pageNum).get()

}