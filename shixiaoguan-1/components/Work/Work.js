// components/Work/Work.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    works: {
      type: Array,
      value: []
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    titleObj: {
      title: "福娃展览",
      link: ""
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
