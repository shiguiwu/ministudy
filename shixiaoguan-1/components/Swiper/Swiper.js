// components/Swiper/Swiper.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    swiperList: [
      "https://stomall-shiguiwu.oss-cn-shenzhen.aliyuncs.com/2020-07-19/%E7%9F%B3%E5%B0%8F%E5%85%89/banner1.png",
      "https://stomall-shiguiwu.oss-cn-shenzhen.aliyuncs.com/2020-07-19/%E7%9F%B3%E5%B0%8F%E5%85%89/banner2.png",
      "https://stomall-shiguiwu.oss-cn-shenzhen.aliyuncs.com/2020-07-19/%E7%9F%B3%E5%B0%8F%E5%85%89/banner3.png",
      "https://stomall-shiguiwu.oss-cn-shenzhen.aliyuncs.com/2020-07-19/%E7%9F%B3%E5%B0%8F%E5%85%89/banner4.png",
    ],
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
