Page({

  /**
   * 页面的初始数据
   */
  data: {
  
    majorList: [
      {image: "/images/kecheng1.png",title: "能不能做",link: "/pages/ps/ps"},
      {image: "/images/caneat.png",title: "能不能吃",link: "/pages/404/404"},
      {image: "/images/preg_weekly.png",title: "订单记录",link: "/pages/order/order"},
      {image: "/images/yimiao_list.png",title: "育苗记录",link: "/pages/404/404"},
      {image: "/images/kecheng3.png",title: "手机防隐",link: "/pages/404/404"}
    ],
    works: [
      {image: "/images/2001.jpg",title: "UI设计作品",desc: "ui design works" ,link: "/pages/404/404"},
      {image: "https://stomall-shiguiwu.oss-cn-shenzhen.aliyuncs.com/2020-07-19/%E7%9F%B3%E5%B0%8F%E5%85%89/xszp4.jpg",title: "室内设计作品",desc: "Interior design works" ,link: "/pages/404/404"},
      {image: "https://stomall-shiguiwu.oss-cn-shenzhen.aliyuncs.com/2020-07-19/%E7%9F%B3%E5%B0%8F%E5%85%89/xszp2.jpg",title: "平面设计作品",desc: "Graphic design workss" ,link: "/pages/404/404"},
      {image: "https://stomall-shiguiwu.oss-cn-shenzhen.aliyuncs.com/2020-07-19/%E7%9F%B3%E5%B0%8F%E5%85%89/xszp7.jpg",title: "网页设计作品",desc: "Web design works" ,link: "/pages/404/404"},
      {image: "https://stomall-shiguiwu.oss-cn-shenzhen.aliyuncs.com/2020-07-19/%E7%9F%B3%E5%B0%8F%E5%85%89/xszp4.jpg",title: "电商设计作品",desc: "E-commerce design works",link: "/pages/404/404"},
      {image: "https://stomall-shiguiwu.oss-cn-shenzhen.aliyuncs.com/2020-07-19/%E7%9F%B3%E5%B0%8F%E5%85%89/xszp5.jpg",title: "影视后期作品",desc: "Later works of film and television",link: "/pages/404/404"},
      {image: "https://stomall-shiguiwu.oss-cn-shenzhen.aliyuncs.com/2020-07-19/%E7%9F%B3%E5%B0%8F%E5%85%89/xszp8.jpg",title: "建筑景观作品",desc: "Architectural landscape works",link: "/pages/404/404"},
      {image: "https://stomall-shiguiwu.oss-cn-shenzhen.aliyuncs.com/2020-07-19/%E7%9F%B3%E5%B0%8F%E5%85%89/xszp6.jpg",title: "三维仿真作品",desc: "3D simulation works",link: "/pages/404/404"}
    ],
    titleObj: {
      title: '动态行业',
      link: ''
    },
    dynamicList: []
  },
  

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  getList() {
    wx.request({
      url: 'https://ku.qingnian8.com/school/list.php',
      success: res => {
        // console.log(res);
        // this.dynamicList = res.data
        this.setData({
          dynamicList: res.data
        })
      }
    })
  }

})