const http = require('../../utils/http.js');

Page({
  data: {
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    nickname: '',
    countOrder: {},
    countMonth: {},
    logs: [],
    dayList: [],
    list: [],
    activeIndex: 0,
    index: 0,
    sum: [{
        title: '今日出单量',
        val: '0'
      },
      {
        title: '本月累积出单量',
        val: '0'
      },
      {
        title: '近三个月出单量',
        val: '0分钟'
      },
      {
        title: '近六个月出单量',
        val: '0分钟'
      }
    ],
    cateArr: [{
        icon: 'work',
        text: '工作'
      },
      {
        icon: 'study',
        text: '学习'
      },
      {
        icon: 'think',
        text: '思考'
      },
      {
        icon: 'write',
        text: '写作'
      },
      {
        icon: 'sport',
        text: '运动'
      },
      {
        icon: 'read',
        text: '阅读'
      }
    ]
  },
  getCountOrder: function () {
    let userInfo = wx.getStorageSync('userInfo')
    if (!userInfo.nickName) {
      wx.showModal({
        title: '温馨提示',
        content: '登录才可查看统计',
        success: (res)=> {
          if (res.confirm) {
            console.log('用户点击确定')
            wx.navigateTo({
              url: '/pages/login/login'
            })
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    } else {
      wx.showLoading();
      let url = `/app/order/count?nickname=${this.data.nickname}`
      let method = 'GET'
      const param = {
        url,
        method,
        callBack: (res) => {
          wx.hideLoading();
          // console.log(res);
          this.getCountMonth()
          this.setData({
            nickname: userInfo.nickName,
            countOrder: res.data
          })
        }
      }
      //发送请求
      http.request(param)
    }
  },

  getCountMonth: function () {
    let url = `/app/order/countMonth?nickname=${this.data.nickname}`
    let method = 'GET'
    const param = {
      url,
      method,
      callBack: (res) => {
        this.setData({
          countMonth: res.data
        })
      }
    }
    //发送请求
    http.request(param)




  },
  onShow: function () {
    this.getCountOrder();
   
  },
  changeType: function (e) {
    var index = e.currentTarget.dataset.index;
    if (index == 0) {
      this.setData({
        list: this.data.dayList
      })
    } else if (index == 1) {
      this.setData({
        list: this.data.logs
      })
    }
    this.setData({
      activeIndex: index
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    wx.showShareMenu({
      withShareTicket: true
    })
  }
})