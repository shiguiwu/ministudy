// pages/guide/guide.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    content: [
      {
        nav: '1，首页',
        small: '/images/prompt.png',
        fun: '本月提成金额：当月内客户出单的佣金所得，没出一个单，佣金都会有所变化。本月总金额：指当月出单的总金额，同理上一个月总金额。然后就是订单列表，可以点击订单修改，也可以点击新增一笔订单',
        small: '/images/prompt.png',
        fun1: '注意：可以下拉订单列表，越是最新的订单越靠前！',
        pic: 'https://stomall-shiguiwu.oss-cn-shenzhen.aliyuncs.com/shixiaoguangmini/index.png'
      },
      {
        nav: ' 2，编辑订单',
        small: '/images/prompt.png',
        fun: '在某宝删复制对应的订单号，日期是默认今天的日期，客户需求：就是客户需要解决的问题或者要实现的功能。拍单金额：就是客户下单的金额，一般和技术价格是一样，特别情况请求咨询管理员。技术名称：就是接单技术的名称。可以是手机号可以是扣扣昵称也可以是扣扣号。技术提成：默认是80%，多次或者是会有、回头客可以是85%，主要要修改默认值。学生提成：可以是学生的价格，目前学生市场很难打开，可以空着不填，或者是在第三方店铺里下单的，默认给他5%，填5即可。技术支付宝：最后是技术支付宝，方便给技术结账，统一有店主结账。',
        small: '/images/prompt.png',
        fun1: '下部红色字体内容：是根据填写，自动计算出来的结果。方便结账。注意：如果是客户付款了，请点击付款按钮，如果是技术已经完成了，请点击完成按钮，如果是已经给技术结账了，请点击结账按钮。',
        pic: 'https://stomall-shiguiwu.oss-cn-shenzhen.aliyuncs.com/shixiaoguangmini/save.png'
      },
      {
        nav: '3，出单分类',
        small: '/images/prompt.png',
        fun: '这里是方便的用于查询订单列表，默认是查询全部，也可以点击付款，完成，结算等状态，进行更精确的查询。',
        small: '/images/prompt.png',
        fun1: '注意：这里只能修改订单，不能添加订单，如果要添加请去首页添加一笔订单！',
        pic: 'https://stomall-shiguiwu.oss-cn-shenzhen.aliyuncs.com/shixiaoguangmini/list.png'
      },
      {
        nav: '4，出单统计',
        small: '/images/prompt.png',
        fun: '这里是更直观的查看你最近出来的情况，单位是笔数',
        small: '/images/prompt.png',
        fun1: '注意：下面一个模块是根据状态统计，三个状态应该加起来等于一个总数',
        pic: 'https://stomall-shiguiwu.oss-cn-shenzhen.aliyuncs.com/shixiaoguangmini/tongji.png'
      },
      {
        nav: '5，个人中心',
        small: '/images/prompt.png',
        fun: '操作指引：不说了，就是我现在在写这个东西。关系作者：如果对本产品有什么建议和意见，都可以联系我，欢迎参与，非诚勿扰！联系客户：就是联系微信客户还是我了。',
        small: '/images/prompt.png',
        fun1: '注意：你也可以直接在反馈意见举报我违规行为，如果有什么不正当行为，请及时批评指正，感谢有你，感恩有你！',
        pic: 'https://stomall-shiguiwu.oss-cn-shenzhen.aliyuncs.com/shixiaoguangmini/me.png'
      }
    ]
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    wx.showShareMenu({
      withShareTicket: true
    })
  }
})